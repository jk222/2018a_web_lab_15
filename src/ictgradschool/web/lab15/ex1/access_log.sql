DROP TABLE IF EXISTS access_log;

CREATE TABLE IF NOT EXISTS access_log(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    description TEXT,
    timeDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);

INSERT INTO access_log(name, description) VALUES

    ('James', 'Donec rutrum augue vitae lectus iaculis consectetur. Donec ornare aliquet augue, a volutpat neque blandit in. Aliquam imperdiet tristique nisi, vel sodales arcu maximus sit amet.'),
    ('Janet','n et dolor nec nulla finibus fermentum sed non quam. Nam cursus elementum risus, non facilisis mauris luctus at. Maecenas placerat dolor orci, sed volutpat neque consequat ut.'),
    ('Tim','Nunc gravida lobortis lectus quis varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam vel nibh sem. Etiam commodo elit nisi.');

SELECT * FROM access_log;
