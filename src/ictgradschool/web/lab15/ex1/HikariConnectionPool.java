package ictgradschool.web.lab15.ex1;

import com.zaxxer.hikari.HikariDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class HikariConnectionPool {
    private static HikariDataSource hds;

    static {

        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }


        hds = new HikariDataSource();
        hds.setJdbcUrl(dbProps.getProperty("url"));
        hds.setDriverClassName(dbProps.getProperty("driver"));
        hds.addDataSourceProperty("useSSL", dbProps.getProperty("useSSL"));
        hds.setUsername(dbProps.getProperty("user"));
        hds.setPassword(dbProps.getProperty("password"));
        hds.setMaximumPoolSize(2);
    }

    public static Connection getConnection() throws SQLException {
        return hds.getConnection();

    }

}
