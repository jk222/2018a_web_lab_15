package ictgradschool.web.lab15.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AccessLogDAO {
    static Properties dbProps = new Properties();

    static {
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<AccessLog> getAllEntries(String usersEntryName){
        List<AccessLog> accessLogList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT id, name, description, timeDate " +
                    "FROM access_log WHERE name LIKE ?;")) {
                stmt.setString(1,"%"+usersEntryName+"%");

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        AccessLog accessLogExample = new AccessLog();

                        accessLogExample.setId(r.getInt(1));
                        accessLogExample.setName(r.getString(2));
                        accessLogExample.setDescription(r.getString(3));
                        accessLogExample.setTimeDate(r.getTimestamp(4));
                        accessLogList.add(accessLogExample);
                    }
                }
            }
        } catch (SQLException e) {
            return new ArrayList<AccessLog>();
        }

        return accessLogList;

    }
/*
    public static List<AccessLog> insertAllEntries(String usersEnteredData){
        List<AccessLog> accessLogList = new ArrayList<>();
*/

}
