package ictgradschool.web.lab15.ex1;

import java.sql.Timestamp;

public class AccessLog {

    private int id;
    private String name;
    private String description;
    private Timestamp timeDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(Timestamp timeDate) {
        this.timeDate = timeDate;
    }

    @Override
    public String toString() {
        return "AccessLog{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", timeDate=" + timeDate +
                '}';
    }
}
