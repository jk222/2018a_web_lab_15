package ictgradschool.web.lab15.ex1;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Ex01Programme {

    public static void main(String[] args) {

        System.out.println("");
        System.out.println("Enter a name: ");

        String usersEntryTitle = Keyboard.readInput();

        List<AccessLog> allEntries = AccessLogDAO.getAllEntries(usersEntryTitle);
//
        if (allEntries.size() > 0) {
            for (AccessLog accessLogExample : allEntries) {
                System.out.println(accessLogExample);
            }

            // We were successful, exit
            //break;
        } else {
            System.out.println("No article found with a title containing '" + usersEntryTitle + "'");
        }
//
        try (Connection connection = HikariConnectionPool.getConnection()) {
            System.out.println("Connection Successful");

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection Failed");
        }
    }
}
